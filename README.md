# **Software Studio 2021 Spring Midterm Project**
## **Notice**
* Replace all [xxxx] to your answer

## **Topic**
* Project Name : midterm_chatroom

## **Basic Components**
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## **Advanced Components**
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# **作品網址：https://midterm-108071003.web.app**

# **Website Detail Description :**
<img src="sign.png" />  
<img src="chatroom page.png" />  



# **Components Description :** 
**Basic components**  
---
1. Membership Mechanism :  
&emsp;&emsp;In the Signin Page, user can signup a new account or signin when already having an account.  
&emsp;&emsp;If user wants to signup a new account, remember to input "your name", which will be a nickname for this account.  
&emsp;&emsp;If user signin successfully, the website will redirect to the Chatroom Page.  

2. Firebase Page :  
&emsp;&emsp; Search for the given url, user can find my Chatroom Page hosting by Firebase.  

3. Database :  
&emsp;&emsp;Please refer to the picture below (just for example).  
Write membership data:  
    ```javascript
    var loginUser = firebase.auth().currentUser;
    firebase.database().ref('users/' + loginUser.uid).set({
        email: loginUser.email,
        name: txtText.value,
        room: ''
    }).catch(function(error){
        console.error("寫入使用者資訊錯誤",error);
    });
    ```  
    <img src="user.jpg" />  

    &emsp;&emsp;Other useful data(ex.room info) can also be Read/Write in authenticated way.  

4. RWD :  
&emsp;&emsp;Website can work fine on different size device.  

5. Topic Key Function :  
&emsp;&emsp;(1) Private Room :  
&emsp;&emsp;&emsp;&emsp;在「Add room name輸入欄位」，輸入想要的房間名字，並按下「+按鈕」即可加入至該房間。  
&emsp;&emsp;&emsp;&emsp;本網站以「房間名」為辨認目標，亦即a若想和b私人聊天，必須先講好一個只有a,b彼此知道的房間名，並各自加入。  
&emsp;&emsp;(2) Send messages in chatroom :  
&emsp;&emsp;&emsp;&emsp;選定房間後，在文字輸入欄輸入欲傳送之文字，並按下「深藍色圓形 發送紐」，即可發送文字至該聊天室。  
&emsp;&emsp;&emsp;&emsp;P.S.本網站支援換行。  
&emsp;&emsp;(3) Load all history message of current chatroom
 :  
&emsp;&emsp;&emsp;&emsp;聊天欄位內會印出使用者所選的聊天室 之 歷史聊天紀錄。  

**Advanced components**  
---
1. Use CSS Animation :  
&emsp;&emsp;The animation only appear in the signin page, please refer to the picture below.  
&emsp;&emsp;<img src="animation.jpg" />  
&emsp;&emsp;The square moves left and right.  

2. Security Report :  
&emsp;&emsp;When sending html code, the website won't breakdown.  
&emsp;&emsp;Please refer to the picture below (just for example).  
&emsp;&emsp;<img src="message.jpg" />  







# **Other Functions Description :** 
1. 離開聊天室 :  
&emsp;&emsp;在欲離開的聊天室內按下「Exit 按鈕」，即可離開該聊天室。  
&emsp;&emsp;註 : 不能離開 public room.

2. nickname :  
&emsp;&emsp;註冊帳號時，可以取暱稱。  

3. 最後訊息 :  
&emsp;&emsp;左側會顯示各個聊天室的最後訊息。

4. 時間 :  
&emsp;&emsp;每則訊息會顯示發送時間。

5. scroll down :  
&emsp;&emsp;自己傳送訊息 或 其他人傳送訊息，聊天室會自動scroll 到最底部。

6. 換行 :  
&emsp;&emsp;在textarea換行，傳送出的訊息也會換行。  

7. 聊天室順序 :  
&emsp;&emsp;發送訊息後，當前聊天室會跳至列表最上方。  

8. public room :  
&emsp;&emsp;提供公共聊天室。





