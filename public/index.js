/*document.addEventListener('DOMContentLoaded', function() {
    if (!Notification) {
      alert('Desktop notifications not available in your browser. Try Chromium.');
      return;
    }
  
    if (Notification.permission !== "granted")
      Notification.requestPermission();
  });
  
  function notifyMe() {
    if (Notification.permission !== "granted")
      Notification.requestPermission();
    else {
      var notification = new Notification('Notification title', {
        icon: 'http://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
        body: "New message!",
      });
  
      notification.onclick = function() {
        window.open("http://stackoverflow.com/a/13328397/1269037");
      };
    }
  }*/


  

var which_list = 'com_list';

var month = new Array(12);
month[0] = "Jan";
month[1] = "Feb";
month[2] = "Mar";
month[3] = "Apr";
month[4] = "May";
month[5] = "Jun";
month[6] = "Jul";
month[7] = "Aug";
month[8] = "Sep";
month[9] = "Oct";
month[10] = "Nov";
month[11] = "Dec";

var private_1 = "<div class='chat_list' id=";
//chat_room_name
var private_2 = " onclick=\"click_person(";
//chat room name
var private_3 = ")\"><div class='chat_people'><div class='chat_img'> <img src='profile.png' alt='sunil'> </div><div class='chat_ib'><h5>";
//chat room name
var private_4 = "<span class='chat_date'>";
//date
var private_5 = "</span></h5><p>";
//p
var private_6 = "</p></div></div></div>\n";


var my_self_1 = "<div class='outgoing_msg'><div class='sent_msg'><p>";
//content in p
var my_self_2 = "</p><span class='time_date'>";
//date info
var my_self_3 = "</span></div></div>\n"; 
            
var others_1 = "<div class='incoming_msg'><div class='incoming_msg_img'><img src='profile.png' alt='sunil'></div><div class='received_msg'><div class='received_withd_msg'><span class='other_email'>";
//email
var others_2 = "</span><p>";
//content in p
var others_3 = "</p><span class='time_date'>";
//date info
var others_4 = "</span></div></div></div>\n";


function message_area() {
    firebase.auth().onAuthStateChanged(function(user) {
        user_email = user.email;

        var postsRef = firebase.database().ref(which_list);
        var total_post = [];
        var first_count = 0;
        var second_count = 0;

        ///測試中 0422 12:05
        if(which_list == 'com_list')
            document.getElementById('printroom').innerHTML = '|| public room ||';
        else
            document.getElementById('printroom').innerHTML = '|| '+ which_list +' ||';
        ///測試中 0422 12:05
            
        postsRef.once('value')
            .then(function(snapshot) {
            
                snapshot.forEach(function(childshot) {
                    var childData = childshot.val();
                    if(childData.email == user_email){ //myself
                        total_post[total_post.length] = my_self_1 + childData.data + my_self_2 + childData.date + my_self_3;
                        first_count += 1;
                    }
                    else{ //others
                        total_post[total_post.length] = others_1 + childData.name + others_2 + childData.data + others_3 + childData.date + others_4;
                        first_count += 1;
                    }
                });
        
                document.getElementById('history').innerHTML = total_post.join('');
                ///測試中
                var element = document.getElementById('history');
                element.scrollTop = element.scrollHeight;
                ///測試中
            
                postsRef.on('child_added', function(data) {
                    second_count += 1;
                    if (second_count > first_count) {
                        var childData = data.val();
                        if(childData.email == user_email){ //myself
                            total_post[total_post.length] = my_self_1 + childData.data + my_self_2 + childData.date + my_self_3; 
                        }
                        else total_post[total_post.length] = others_1 + childData.name + others_2 + childData.data + others_3 + childData.date + others_4;
                        document.getElementById('history').innerHTML = total_post.join('');

                        ///測試中
                        var element = document.getElementById('history');
                        element.scrollTop = element.scrollHeight;
                        ///測試中
                        if(which_list != 'com_list'){
                            var el = document.getElementById(which_list);
                            el.remove();
    
                            firebase.database().ref('/users/' + user.uid).once('value').then(function(snapshot){
    
                                var list = snapshot.val().room.split(/\s+/);
                    
                                
    
                                    (async function loop() { 
                                        for(i=0 ; i<list.length ; i++){
    
                                            if(list[i]!=which_list) continue;
    
                                            var paragraph = '';
                                            
                                            var roomRef = firebase.database().ref(list[i]);
                                            await roomRef.limitToLast(1).once('value').then(function(snapshot){
                                                
                                                snapshot.forEach(function(item){
                                                    if(item.val().data.length>72)
                                                        paragraph = item.val().data.substr(0,72);
                                                    else 
                                                        paragraph = item.val().data;
                                                    
                                            
                                                    document.getElementById('choose_chat_room').innerHTML = private_1 + '\'' + list[i] + '\'' + private_2 + '\'' + list[i] + '\'' + private_3 + list[i] + private_4 + item.val().date + private_5 + paragraph + private_6 + document.getElementById('choose_chat_room').innerHTML;
                                                })
                                            });
                                        }
                                    })(); 
                                
                            })
    
                        }
                    }
                    
                });
            
            })
            .catch(e => console.log(e.message));
    });
}


function exit() {
    var cur_user = firebase.auth().currentUser;
    if(cur_user){
    firebase.database().ref('users/' + cur_user.uid)
    .once('value').then(function(snapshot) {
        if(which_list == 'com_list'){
            alert('you cannot exit public room!')
        }
        else{
            var txt = snapshot.val().room;
            var nickname = snapshot.val().name;
            txt = txt.replace(which_list,'');
            if(txt==' ') txt='';

            firebase.database().ref('users/' + cur_user.uid).update({
                room:  txt
            });

            firebase.database().ref(which_list).push({
                data: nickname + ' 退出聊天室',
                email: 'default@gmail.com',
                date: new Date().getHours() + ":" + new Date().getMinutes() + "    |    "+ month[new Date().getMonth()] + " " + new Date().getDate(),
                name: '系統訊息'
            })

            var el = document.getElementById(which_list);
            el.remove();
            which_list = 'com_list';
            document.getElementById('history').innerHTML = ''; //切換聊天室的時候 訊息內容清空
            message_area();
        }
    })
    }
}



function click_person(a) {
    var add_room = document.getElementById('newroom');
    var nickname = '';

    
    if(a=='public'){
        which_list = 'com_list';
        document.getElementById('history').innerHTML = ''; //切換聊天室的時候 訊息內容清空
        message_area();
    }
    else if(a == 'button'){ //新增聊天室
        if(add_room.value != ''){
            var reco = add_room.value;

            //notifyMe()
            
            var cur_user = firebase.auth().currentUser;
            var char;
            
            firebase.database().ref('users/' + cur_user.uid)
            .once('value').then(function(snapshot) {
                    char = snapshot.val().room;
                    nickname = snapshot.val().name;

                    var txxt;
                    if(char=='') txxt = '';
                    else  txxt = char+' ';

                    firebase.database().ref('users/' + cur_user.uid).update({
                        room:  txxt + reco
                    });


                    firebase.database().ref(reco).push({
                        data: nickname + ' 加入聊天室',
                        email: 'default@gmail.com',
                        date: new Date().getHours() + ":" + new Date().getMinutes() + "    |    "+ month[new Date().getMonth()] + " " + new Date().getDate(),
                        name: '系統訊息'
                    })


            }).then(() => {
                
                    firebase.database().ref('/users/' + cur_user.uid).once('value').then(function(snapshot){

                        var list = snapshot.val().room.split(/\s+/);
                        console.log(list);
                        
                        

                            (async function loop() { 
                                for(i=0 ; i<list.length ; i++){

                                    var paragraph = '';

                                    if(list[i] == reco){
                                        var roomRef = firebase.database().ref(list[i]);
                                        await roomRef.limitToLast(1).once('value').then(function(snapshot){
                                            
                                            snapshot.forEach(function(item){
                                                if(item.val().data.length>72)
                                                    paragraph = item.val().data.substr(0,72);
                                                else 
                                                    paragraph = item.val().data;
                                                
                                        
                                                document.getElementById('choose_chat_room').innerHTML += private_1 + '\'' + list[i] + '\'' + private_2 + '\'' + list[i] + '\'' + private_3 + list[i] + private_4 + item.val().date + private_5 + paragraph + private_6;
                                            })
                                        });
                                    }
                                }
                            })(); 
                        
                    })
            })
            .catch(e => console.log(e.message));

            

            add_room.value = '';
        }
    }
    else{ 
        which_list = a;
        document.getElementById('history').innerHTML = '';
        message_area();
    }
}


function init() {

    var post_btn = document.getElementById('message_send');
    var post_txt = document.getElementById('writing_msg');
    
    var user_email = '';
    var name = '';

    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            
            var logoutbtn = document.getElementById('logout-btn');
            logoutbtn.addEventListener("click", function() {
                firebase.auth().signOut().then(function() {
                    alert("User sign out success!");
                    user_email = ''; 
                }).catch(function(error) {
                    alert("User sign out failed!");
                })
            }, false);

            //name
            var ref2 = firebase.database().ref('/users/' + user.uid);
                ref2.once('value').then(function(snapshot) {
                    name = snapshot.val().name;
                    console.log(snapshot.val().name);
            })
            .catch(e => console.log(e.message));


            /*我把它移進來了*/
            post_btn.addEventListener('click', function() {
                
                if (post_txt.value != "") {

                    var ss = post_txt.value;
                    ss = ss.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
                    ss = ss.replace(/\n/g , '<br>');
                    
                    
                    var Ref = firebase.database().ref(which_list); 

                    var data = {
                        data: ss,
                        email: user_email,
                        date: new Date().getHours() + ":" + new Date().getMinutes() + "    |    "+ month[new Date().getMonth()] + " " + new Date().getDate(),
                        name: name
                    };
                    Ref.push(data);
                    post_txt.value = "";



                    if(which_list != 'com_list'){
                        var el = document.getElementById(which_list);
                        el.remove();

                        firebase.database().ref('/users/' + user.uid).once('value').then(function(snapshot){

                            var list = snapshot.val().room.split(/\s+/);
                
                            

                                (async function loop() { 
                                    for(i=0 ; i<list.length ; i++){

                                        if(list[i]!=which_list) continue;

                                        var paragraph = '';
                                        
                                        var roomRef = firebase.database().ref(list[i]);
                                        await roomRef.limitToLast(1).once('value').then(function(snapshot){
                                            
                                            snapshot.forEach(function(item){
                                                if(item.val().data.length>72)
                                                    paragraph = item.val().data.substr(0,72);
                                                else 
                                                    paragraph = item.val().data;
                                                
                                        
                                                document.getElementById('choose_chat_room').innerHTML = private_1 + '\'' + list[i] + '\'' + private_2 + '\'' + list[i] + '\'' + private_3 + list[i] + private_4 + item.val().date + private_5 + paragraph + private_6 + document.getElementById('choose_chat_room').innerHTML;
                                            })
                                        });
                                    }
                                })(); 
                            
                        })

                    }

                    var element = document.getElementById('history');
                    element.scrollTop = element.scrollHeight;
                }
            });
        
            
            var postsRef = firebase.database().ref(which_list);
            var total_post = [];
            var first_count = 0;
            var second_count = 0;

            ///測試中 0422 12:05
            if(which_list == 'com_list')
                document.getElementById('printroom').innerHTML = '|| public room ||';
            else
                document.getElementById('printroom').innerHTML = '|| '+ which_list +' ||';
            ///測試中 0422 12:05
        
            postsRef.once('value')
                .then(function(snapshot) {
                    
                    snapshot.forEach(function(childshot) {
                        var childData = childshot.val();
                        if(childData.email == user_email){ //myself
                            total_post[total_post.length] = my_self_1 + childData.data + my_self_2 + childData.date + my_self_3;
                            first_count += 1;
                        }
                        else{ //others
                            total_post[total_post.length] = others_1 + childData.name + others_2 + childData.data + others_3 + childData.date + others_4;
                            first_count += 1;
                        }
                    });
        
                    document.getElementById('history').innerHTML = total_post.join('');

                    ///測試中
                    var element = document.getElementById('history');
                    element.scrollTop = element.scrollHeight;
                    ///測試中
        
                    postsRef.on('child_added', function(data) {
                        second_count += 1;
                        if (second_count > first_count) {
                            var childData = data.val();
                            if(childData.email == user_email){ //myself
                                total_post[total_post.length] = my_self_1 + childData.data + my_self_2 + childData.date + my_self_3; 
                            }
                            else total_post[total_post.length] = others_1 + childData.name + others_2 + childData.data + others_3 + childData.date + others_4;
                            document.getElementById('history').innerHTML = total_post.join('');

                            ///測試中
                            var element = document.getElementById('history');
                            element.scrollTop = element.scrollHeight;
                            ///測試中
                            if(which_list != 'com_list'){
                                var el = document.getElementById(which_list);
                                el.remove();
        
                                firebase.database().ref('/users/' + user.uid).once('value').then(function(snapshot){
        
                                    var list = snapshot.val().room.split(/\s+/);
                        
                                    
        
                                        (async function loop() { 
                                            for(i=0 ; i<list.length ; i++){
        
                                                if(list[i]!=which_list) continue;
        
                                                var paragraph = '';
                                                
                                                var roomRef = firebase.database().ref(list[i]);
                                                await roomRef.limitToLast(1).once('value').then(function(snapshot){
                                                    
                                                    snapshot.forEach(function(item){
                                                        if(item.val().data.length>72)
                                                            paragraph = item.val().data.substr(0,72);
                                                        else 
                                                            paragraph = item.val().data;
                                                        
                                                
                                                        document.getElementById('choose_chat_room').innerHTML = private_1 + '\'' + list[i] + '\'' + private_2 + '\'' + list[i] + '\'' + private_3 + list[i] + private_4 + item.val().date + private_5 + paragraph + private_6 + document.getElementById('choose_chat_room').innerHTML;
                                                    })
                                                });
                                            }
                                        })(); 
                                    
                                })
        
                            }
                        }
                    });
        
        
                })
                .catch(e => console.log(e.message));



            ////////////////////////////////////public
            var public_1 = "<div class='chat_list active_chat' id='public' onclick=\"click_person('public')\"><div class='chat_people'><div class='chat_img'><img src='profile.png' alt='sunil'></div><div class='chat_ib'><h5>Public room<span class='chat_date'>";
            // date
            var public_2 = "</span></h5><p>";
            //p
            var public_3 = "</p></div></div></div>\n";

            var fir = 0;
            var sec = 0;

            var publicRef = firebase.database().ref('com_list');
                
            publicRef.limitToLast(1).once('value')
                .then(function(snapshot) {
                    snapshot.forEach(function(item){
                        fir += 1;
                        if(item.val().data.length>72){
                            document.getElementById('choose_chat_room').innerHTML =  public_1 + item.val().date + public_2 + item.val().data.substr(0,72) +'...' + public_3;
                            
                        }
                        else {
                            document.getElementById('choose_chat_room').innerHTML =  public_1 + item.val().date + public_2 + item.val().data + public_3;
                            
                        }
                    })

                    publicRef.on('child_added', function(item) {
                        var el = document.getElementById('public');
                        sec += 1;
                        if (sec > fir) {
                            if(item.val().data.length>72){
                                el.remove();
                                document.getElementById('choose_chat_room').innerHTML = public_1 + item.val().date + public_2 + item.val().data.substr(0,72) +'...' + public_3 + document.getElementById('choose_chat_room').innerHTML;
                            }
                            else {
                                el.remove();
                                document.getElementById('choose_chat_room').innerHTML = public_1 + item.val().date + public_2 + item.val().data + public_3 + document.getElementById('choose_chat_room').innerHTML;
                            }
                        }
                    });
            });
            ////////////////////////////////////public 



            firebase.database().ref('/users/' + user.uid).once('value').then(function(snapshot){

                var list = snapshot.val().room.split(/\s+/);
                
                

                    (async function loop() { 
                        for(i=0 ; i<list.length ; i++){

                            if(list[i] =='') continue;

                            var paragraph = '';
                            
                            var roomRef = firebase.database().ref(list[i]);
                            await roomRef.limitToLast(1).once('value').then(function(snapshot){
                                
                                snapshot.forEach(function(item){
                                    if(item.val().data.length>72)
                                        paragraph = item.val().data.substr(0,72);
                                    else 
                                        paragraph = item.val().data;
                                    
                            
                                    document.getElementById('choose_chat_room').innerHTML += private_1 + '\'' + list[i] + '\'' + private_2 + '\'' + list[i] + '\'' + private_3 + list[i] + private_4 + item.val().date + private_5 + paragraph + private_6;
                                })
                            });
                        }
                    })(); 
                
            })
        } 
        else{
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('history').innerHTML = "";
            document.getElementById('choose_chat_room').innerHTML = "";
            document.getElementById('printroom').innerHTML = '|| 請登入 ||';
        }
    });
}

window.onload = function() {
    init();
};